<?php

return [
    'key' => env('MORALIS_KEY', 'changeme'),
    'url' => env('MORALIS_URL', 'https://deep-index.moralis.io/api/v2/'),
    'solana-url' => env('SOLANA_URL', 'https://solana-gateway.moralis.io/'),
    'chain' => env('MORALIS_CHAIN', 'eth'),
];
