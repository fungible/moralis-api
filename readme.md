# Fungible Development Moralis Api Package
This package is created to make Moralis Api calls more easy to use.
***
## Table of Contents:
1. [Installation](#installation)
2. [Usage](#usage)
3. [EVM Functions](#evm-api-functions)
    - [Native](#native)
        - [Content by block bash](#getcontentbyblockhash)
        - [Date to block](#getdatetoblock)
        - [Address logs](#getaddresslogs)
        - [Nft transfers by block number or hash](#getblocknfttransfers)
        - [Transactions of block by hash](#gettransactionhash)
        - [Address events](#getaddressevents)
        - [Address function](#runabifunction)
    - [Account](#account)
        - [Native transactions](#getnativetransactions)
        - [Specific address balance](#getspecificaddressbalance)
        - [Specific address token balance](#getspecificaddresstokenbalance)
        - [ERC20 token transactions](#geterc20tokentransactions)
        - [User address NFTS](#getuseraddressnfts)
        - [NFT transfers](#getnfttransfers)
        - [NFT from address](#getnftsfromaddress)
    - [Token](#token) 
        - [ERC20 metadata](#geterc20metadata)
        - [Contract trades](#getcontracttrades)
        - [Get lowest price](#getlowestprice)
        - [Token contract metadata](#gettokencontractmetadata)
        - [Token price](#gettokenprice)
        - [Token contract transactions](#geterc20tokencontracttransactions)
        - [Allowance](#getallowance)
        - [NFT search](#getnftdata)
        - [NFT transfers](#getnfttransferdata)
        - [Contract NFTs](#getcontractnfts)
        - [Contract NFT transfers](#getnftcontracttransfers)
        - [Contract NFT owners](#getallcontractnftowners)
        - [NFT contract metadata](#getglobalmetadata)
        - [Resync NFT metadata](#resyncnftmetadata)
        - [Contract sync](#contractsync)
        - [All NFT data](#getallnftdata)
        - [All NFT owners within a contract](#getallnftownerswithincontract)
        - [Token transfers](#gettokentransfers)
    - [Resolve](#resolve)
        - [Resolve domain](#resolvedomain)
        - [Resolve address](#resolveethaddress)
    - [Defi](#defi)
        - [Pair address liquidity reserves](#getpairaddressliquidityreserves)
        - [Pair address](#getpairaddress)
    - [Storage](#storage)
        - [Upload file to IPFS](#uploadfolder)
    - [Info](#info)
        - [Web3 version](#getweb3version)
        - [Endpoint weights](#getendpointweights)
4. [Solana Functions](#solana-functions)
    - [Account](#account-1)
        - [Address balance](#getsolanaaddressbalance)
        - [Address token](#getsolanaaddresstoken)
        - [Address nft](#getsolanaaddressnft)
        - [Address portfolio](#getsolanaaddressportfolio)
    - [Nft](#nft)
        - [NFT metadata](#getsolananftmetadata)

***
## Installation:
***
Install the latest version of the package by running:
```
composer require fungible/moralis-api
```
Publish the config by running:
```
php artisan vendor:publish
```
Then configure the moralis.php config with your Moralis api key and the chain you want to use.
***

## Usage:
***
To start using the api calls create an api file with the following code:
```php
    public $client;

    public function __construct()
    {
        $this->client = new Moralis();
    }
```
Now you can create functions and call `$this->client->getContentByBlockHash($blockNumber, $subDomain)` or any other function provided below!
***

## EVM Api Functions:

### Native:
***
#### getContentByBlockHash()
    
Gets the contents of a block by block hash

Url: `/block/{block_number_or_hash}`
- Method: `GET`
- Weight: `5 requests`
- Parameters: 
    - Required:
        - string `$blockNumber`
            - The block hash or block number
    - Optional:
        - string `$subDomain`
            - The subdomain of the moralis server to use (Only when selecting local devchain as chain)
***
#### getDateToBlock()

Gets the closest block of the provided date

Url: `/dateToBlock`
- Method: `GET`
- Weight: `2 requests`
- Parameters: 
    - Required:
        - string `$date`
            - Unix date in miliseconds or a datestring (any format that is accepted by momentjs)
    - Optional:
        - string `$providerUrl`
            - web3 provider url to user when using local dev chain
***
#### getAddressLogs()

Gets the logs from an address

Url: `/{address}/logs`
- Method: `GET`
- Weight: `2 requests`
- Parameters: 
    - Required:
        - string `$address`
            - address from contract or wallet
    - Optional:
        - string `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
        - string `$blockNumber`
            - The block number
        - string `$fromBlock`
            - The minimum block number from where to get the logs
        - string `$toBlock`
            - The maximum block number from where to get the logs
        - string `$fromDate`
            - The date from where to get the logs (any format that is accepted by momentjs)
        - string `$toDate`
            - Get the logs to this date (any format that is accepted by momentjs)
        - string `$topic0`
            - Topic0
        - string `$topic1`
            - Topic1
        - string `$topic2`
            - Topic2
        - string `$topic3`
            - Topic 3
        - string `$cursor`
            - The cursor returned in the last resposnse (for getting the next page)
        - string `$limit`
            - Limit
***
#### getBlockNftTransfers()

Gets NFT transfers by block number or block hash

Url: `/block/{block_number_or_hash}/nft/transfers`
- Method: `GET`
- Weight: `2 requests`
- Parameters:
    - Required:
        - string `$blockNumber`
            - The block hash or block number
    - Optional:
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - Limit
***
#### getTransactionHash()

Gets the contents of a block transaction by hash

Url: `/transaction/{transaction_hash}`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$transactionHash`
            - The transaction hash
    - Optional:
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
***
#### getAddressEvents()

Gets the contents of a block transaction by hash

Url: `/{address}/events`
- Method: `POST`
- Weight: `2 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address
        - string: `$topic`
            - The topic of the event
        - array: `$abi`
            - ABI of the specific event
    - Optional:
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain
        - int: `$fromBlock`
            - The minimum block number from where to get the logs
        - int: `$toBlock`
            - The maximum block number from where to get the logs
        - string: `$fromDate`
            - The date from where to get the logs (any format that is accepted by momentjs)
        - string: `$toDate`
            - Get the logs to this date (any format that is accepted by momentjs)
        - int: `$offset`
            - Offset
        - int: `$limit`
            - Limit
***
#### runAbiFunction()

Gets the contents of a block transaction by hash

Url: `/{address}/function`
- Method: `POST`
- Weight: `2 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address
        - string: `$functionName`
            - Function name
        - array: `$abi`
            - Contract ABI
    - Optional:
        - array: `$params`
            - Parameters associated with the abi function
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain

***    
### Account:
***
#### getNativeTransactions()

Gets native transactions in descending order based on block number

Url: `/{address}`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - Address
    - Optional:
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
        - int: `$fromBlock`
            - The minimum block number from where to get the transactions
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - int: `$toBlock`
            - The maximum block number from where to get the transactions.
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$fromDate`
            - The date from where to get the transactions (any format that is accepted by momentjs)
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - string: `$toDate`
            - Get the transactions to this date (any format that is accepted by momentjs)
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getSpecificAddressBalance()

Gets native balance for a specific address

Url: `/{address}/balance`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - The address for which the native balance will be checked
    - Optional:
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain
        - int: `$toBlock`
            - The block number on which the balances should be checked

***
#### getSpecificAddressTokenBalance()

Gets token balances for a specific address

Url: `/{address}/erc20`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - The address for which token balances will be checked
    - Optional:
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
        - int: `$toBlock`
            - The block number on which the balances should be checked
        - array: `$tokenAddresses`
            - The addresses to get balances for

***
#### getERC20TokenTransactions()

Gets ERC20 token transactions in descending order based on block number

Url: `/{address}/erc20/transfers`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - Address
    - Optional:
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
        - int: `$fromBlock`
            - The minimum block number from where to get the transactions
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - int: `$toBlock`
            - The maximum block number from where to get the transactions.
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$fromDate`
            - The date from where to get the transactions (any format that is accepted by momentjs)
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - string: `$toDate`
            - Get the transactions to this date (any format that is accepted by momentjs)
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getUserAddressNfts()

Gets NFTs owned by the given address

Url: `/{address}/nft`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Required:
        - string: `$address`
            - The owner of a given token
    - Optional:
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain
        - int: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - array: `$tokenAddresses`
            - The addresses to get balances for
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getNftTransfers()

Gets the transfers of the tokens matching the given parameters

Url: `/{address}/nft/transfers`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Required:
        - string: `$address`
            - The sender or recepient of the transfer
    - Optional:
        - int: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - string: `$direction`
            - The transfer direction
                - Available values : both, to, from
                - Default value : both
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getNftsFromAddress()

Gets NFTs owned by the given address
- Use the token_address param to get results for a specific contract only
- Note results will include all indexed NFTs
- Any request which includes the token_address param will start the indexing process for that NFT collection the very first time it is requested

Url: `/{address}/nft/{token_address}`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Required:
        - string: `$address`
            - The owner of a given token
        - string: `$tokenAddress`
            - The owner of a given token
    - Optional:
        - int: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
### Token:

***
#### getERC20Metadata()

Returns metadata (name, symbol, decimals, logo) for a given token contract address.

Url: `/erc20/metadata`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - array: `$addresses`
            - The addresses to get metadata for
    - Optional:
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain

***
#### getContractTrades()

Get the nft trades for a given contracts and marketplace

Url: `/nft/{address}/trades`
- Method: `GET`
- Weight: `4 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract
    - Optional:
       - int: `$fromBlock`
            - The minimum block number from where to get the transactions
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - int: `$toBlock`
            - The maximum block number from where to get the transactions.
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$fromDate`
            - The date from where to get the transactions (any format that is accepted by momentjs)
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - string: `$toDate`
            - Get the transactions to this date (any format that is accepted by momentjs)
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain
        - string: `$marketplace`
            - The format of the token id
                - Available values : opensea
                - Default value : opensea
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getLowestPrice()

Get the lowest price found for a nft token contract for the last x days (only trades paid in ETH)

Url: `/nft/{address}/lowestprice`
- Method: `GET`
- Weight: `4 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract
    - Optional:
        - int: `$days`
            - The number of days to look back to find the lowest price If not provided 7 days will be the default
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain
        - string: `$marketplace`
            - The format of the token id
                - Available values : opensea
                - Default value : opensea

***
#### getTokenContractMetadata()

Returns metadata (name, symbol, decimals, logo) for a given token contract address.

Url: `/erc20/metadata/symbols`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - array: `$symbols`
            - The symbols to get metadata for
    - Optional:
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)

***
#### getTokenPrice()

Returns the price nominated in the native token and usd for a given token contract address.

Url: `/erc20/{address}/price`
- Method: `GET`
- Weight: `3 requests`
- Parameters:
    - Required:
        - string: `$address`
            - The address of the token contract
    - Optional:
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain
        - string: `$exchange`
            - The factory name or address of the token exchange
        - string: `$toBlock`
            - toBlock

***
#### getERC20TokenContractTransactions()

Gets ERC20 token contract transactions in descending order based on block number

Url: `/erc20/{address}/transfers`
- Method: `GET`
- Weight: `2 requests`
- Parameters:
    - Required:
        - string: `$address`
            - The address of the token contract
    - Optional:
        - string: `$subDomain`
            - The subdomain of the moralis server to use (Only use when selecting local devchain as chain)
       - int: `$fromBlock`
            - The minimum block number from where to get the transactions
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - int: `$toBlock`
            - The maximum block number from where to get the transactions.
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$fromDate`
            - The date from where to get the transactions (any format that is accepted by momentjs)
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - string: `$toDate`
            - Get the transactions to this date (any format that is accepted by momentjs)
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$offset`
            - offset
        - string: `$limit`
            - limit

***
#### getAllowance()

Gets the amount which the spender is allowed to withdraw from the spender

Url: `/erc20/{address}/allowance`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$ownerAddress`
            - The address of the token owner
        - string: `$spenderAddress`
            - The address of the token spender
        - string: `$address`
            - The address of the token contract
    - Optional:
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain

***
#### getNftData()

Returns metadata (name, symbol, decimals, logo) for a given token contract address.

Url: `/nft/search`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Required:
        - array: `$searchQuery`
            - The search string
    - Optional:
        - string: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - string: `$filter`
            - What fields the search should match on. To look into the entire metadata set the value to 'global'. To have a better response time you can look into a specific field like name
                - Available values : name, description, attributes, global, name,description, name,attributes, description,attributes, name,description,attributes
                - Default value : global
        - int: `$fromBlock`
            - The minimum block number from where to get the transactions
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - int: `$toBlock`
            - The maximum block number from where to get the transactions.
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$fromDate`
            - The date from where to get the transactions (any format that is accepted by momentjs)
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - string: `$toDate`
            - Get the transactions to this date (any format that is accepted by momentjs)
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$addresses`
            - The addresses to get metadata for
        - int: `$tokenAddress`
            - tokenAddress
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getNftTransferData()

Gets the transfers of the tokens from a block number to a block number

Url: `/nft/transfers`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Optional:
        - int: `$fromBlock`
            - The minimum block number from where to get the transactions
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - int: `$toBlock`
            - The maximum block number from where to get the transactions.
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$fromDate`
            - The date from where to get the transactions (any format that is accepted by momentjs)
                - Provide the param `$fromBlock` or `$fromDate`
                - If `$fromDate` and `$fromBlock` are provided, `$fromBlock` will be used.
        - string: `$toDate`
            - Get the transactions to this date (any format that is accepted by momentjs)
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getContractNfts()

Gets data, including metadata (where available), for all token ids for the given contract address.

Url: `/nft/{address}`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract
    - Optional:
        - string: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getNftContractTransfers()

Gets the transfers of the tokens matching the given parameters

Url: `/nft/{address}/transfers`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract
    - Optional:
        - string: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getAllContractNftOwners()

Gets all owners of NFT items within a given contract collection

Url: `/nft/{address}/owners`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract
    - Optional:
        - string: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getGlobalMetadata()

Gets the contract level metadata (name, symbol, base token uri) for the given contract
- Requests for contract addresses not yet indexed will automatically start the indexing process for that NFT collection

Url: `/nft/{address}/metadata`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract

***
#### resyncNftMetadata()

ReSync the metadata for an NFT
- The metadata flag will request a the NFT's metadata from the already existing token_uri
- The uri(default) flag will fetch the latest token_uri from the given NFT address. In sync mode the metadata will also be fetched
- The sync mode will make the endpoint synchronous so it will wait for the task to be completed before responding
- The async mode(default) will make the endpoint asynchronous so we will wait for the task to be completed before responding

Url: `/nft/{address}/{token_id}/metadata/resync`
- Method: `GET`
- Weight: `5 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract
        - string: `$tokenId`
            - The id of the token
    - Optional:
        - string: `$flag`
            - The type of resync to operate
                - Available values : uri, metadata
                - Default value : uri
        - string: `$mode`
            - To define the behaviour of the endpoint
                - Available values : async, sync
                - Default value : async

***
#### contractSync()

Sync a Contract for NFT Index

Url: `/nft/{address}/sync`
- Method: `PUT`
- Weight: `5 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract

***
#### getAllNftData()

Gets data, including metadata (where available), for the given token id of the given contract address.

Url: `/nft/{address}/{token_id}`
- Method: `GET`
- Weight: `2 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract
        - string: `$tokenId`
            - The id of the token
    - Optional:
        - string: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal

***
#### getAllNftOwnersWithinContract()

Gets all owners of NFT items within a given contract collection
- Use after /nft/contract/{token_address} to find out who owns each token id in a collection
- Requests for contract addresses not yet indexed will automatically start the indexing process for that NFT collection

Url: `/nft/{address}/{token_id}/owners`
- Method: `GET`
- Weight: `20 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract
        - string: `$tokenId`
            - The id of the token
    - Optional:
        - string: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
#### getTokenTransfers()

Gets the transfers of the tokens matching the given parameters

Url: `/nft/{address}/{token_id}/tranfers`
- Method: `GET`
- Weight: `2 requests`
- Parameters:
    - Required:
        - string: `$address`
            - Address of the contract
        - string: `$tokenId`
            - The id of the token
    - Optional:
        - string: `$format`
            - The format of the token id
                - Available values : decimal, hex
                - Default value : decimal
        - string: `$order`
            - The field(s) to order on and if it should be ordered in ascending or descending order. Specified by: fieldName1.order,fieldName2.order. Example 1: "block_number", "block_number.ASC", "block_number.DESC", Example 2: "block_number and contract_type", "block_number.ASC,   contract_type.DESC"
        - string: `$cursor`
            - The cursor returned in the last response (for getting the next page)
        - int: `$limit`
            - limit

***
### Resolve:

***
#### resolveDomain()

Resolves an Unstoppable domain and returns the address

Url: `/resolve/{domain}`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$domain`
            - Domain to be resolved
    - Optional:
        - string: `$currency`
            - The currency to query
                - Available values : eth, 0x1
                - Default value : eth

***
#### resolveEthAddress()

Resolves an ETH address and find the ENS name

Url: `/resolve/{address}/reverse`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - The address to be resolved

***
### Defi:

***
#### getPairAddressLiquidityReserves()

Get the liquidity reserves for a given pair address. Only Uniswap V2 based exchanges supported at the moment.

Url: `/{pair_address}/reserves`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$pairAddress`
            - Liquidity pair address
    - Optional:
        - string: `$toBlock`
            - To get the reserves at this block number
        - string: `$toDate`
            - Get the transactions to this date (any format that is accepted by momentjs)
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.
        - string: `$providerUrl`
            - web3 provider url to user when using local dev chain

***
#### getPairAddress()

Fetches and returns pair data of the provided token0+token1 combination. The token0 and token1 options are interchangable (ie. there is no different outcome in "token0=WETH and token1=USDT" or "token0=USDT and token1=WETH")

Url: `/{token0_address}/{token1_address}/pairAddress`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$token0_address`
            - Token0 address
        - string: `$token1_address`
            - Token1 address
        - string: `$exchange`
            - The factory name or address of the token exchange
                - Available values : uniswapv2, uniswapv3, sushiswapv2, pancakeswapv2, pancakeswapv1, quickswap
    - Optional:
        - string: `$toBlock`
            - To get the reserves at this block number
        - string: `$toDate`
            - Get the transactions to this date (any format that is accepted by momentjs)
                - Provide the param `$toBlock` or `$toDate`
                - If `$toDate` and `$toBlock` are provided, `$toBlock` will be used.

***
### Storage:

***
#### uploadFolder()

Uploads multiple files and place them in a folder directory

Url: `/ipfs/uploadFolder`
- Method: `POST`
- Parameters:
    - Optional:
        - Request body: `$data`
            - Array of JSON and Base64 Supported

***
### Info:

***
#### getWeb3Version()

Url: `/web3/version`
- Method: `GET`
- Parameters:
    - No parameters

***
#### getEndpointWeights()

Url: `/info/endpointWeights`
- Method: `GET`
- Parameters:
    - No parameters

***
## Solana Functions:

### Account:

***
#### getSolanaAddressBalance()

Url: `/account/{network}/{address}/balance`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - Address
        - string: `$network`
            - Available values : mainnet, devnet

***
#### getSolanaAddressToken()

Url: `/account/{network}/{address}/tokens`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - Address
        - string: `$network`
            - Available values : mainnet, devnet

***
#### getSolanaAddressNft()

Url: `/account/{network}/{address}/nft`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - Address
        - string: `$network`
            - Available values : mainnet, devnet

***
#### getSolanaAddressPortfolio()

Url: `/account/{network}/{address}/portfolio`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - Address
        - string: `$network`
            - Available values : mainnet, devnet

***
### Nft:

***
#### getSolanaNftMetadata()

Url: `/account/{network}/{address}/metadata`
- Method: `GET`
- Weight: `1 request`
- Parameters:
    - Required:
        - string: `$address`
            - Address
        - string: `$network`
            - Available values : mainnet, devnet
