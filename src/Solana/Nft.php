<?php

namespace Fungible\MoralisApi\Solana;

trait Nft {
    /**
     * @param string $address
     * @param string $network
     * 
     * @return array|null
     */
    public function getSolanaNftMetadata(string $address, string $network = 'mainnet')
    {
        try {
            $response = $this->get(config('moralis.solana-url'). 'nft/'. $network. '/'. $address. 'metadata');
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }
}