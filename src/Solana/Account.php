<?php

namespace Fungible\MoralisApi\Solana;

trait Account {
    /**
     * @param string $address
     * @param string $network
     * 
     * @return array|null
     */
    public function getSolanaAddressBalance(string $address, string $network = 'mainnet')
    {
        try {
            $response = $this->get(config('moralis.url'). 'account/'. $network. '/'. $address. '/balance');
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $network
     * 
     * @return array|null
     */
    public function getSolanaAddressToken(string $address, string $network = 'mainnet')
    {
        try {
            $response = $this->get(config('moralis.solana-url'). 'account/'. $network. '/'. $address. '/tokens');
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $network
     * 
     * @return array|null
     */
    public function getSolanaAddressNft(string $address, string $network = 'mainnet')
    {
        try {
            $response = $this->get(config('moralis.solana-url'). 'account/'. $network. '/'. $address. '/nft');
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $network
     * 
     * @return array|null
     */
    public function getSolanaAddressPortfolio(string $address, string $network = 'mainnet')
    {
        try {
            $response = $this->get(config('moralis.solana-url'). 'account/'. $network. '/'. $address. '/portfolio');
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }
}