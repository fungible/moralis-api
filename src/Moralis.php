<?php

namespace Fungible\MoralisApi;

use GuzzleHttp\Client;
use App\DataTransferObjects\Token;
use Fungible\MoralisApi\EVM\V3\Account;
use Fungible\MoralisApi\EVM\V3\Defi;
use Fungible\MoralisApi\EVM\V3\Info;
use Fungible\MoralisApi\EVM\V3\Native;
use Fungible\MoralisApi\EVM\V3\Resolve;
use Fungible\MoralisApi\EVM\V3\Storage;
use Fungible\MoralisApi\EVM\V3\Token as V3Token;
use Fungible\MoralisApi\Solana\Account as SolanaAccount;
use Fungible\MoralisApi\Solana\Nft;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection;


class Moralis extends Client
{
    // Web3 API
    use Account;
    use Defi;
    use Info;
    use Native;
    use Resolve;
    use Storage;
    use V3Token;

    //Solana API
    use SolanaAccount;
    use Nft;

    public function __construct(array $config = [])
    {
        $url = config('moralis.url');
        $key = config('moralis.key');

        parent::__construct(array_merge($config, [
            'base_uri' => $url,
            'headers' => [
                'x-api-key' => $key,
            ],
        ]));
    }

    /**
     * @param array $options
     * @return string
     */
    public function getQuery($options = []){
        $query = "?chain=" . config('moralis.chain');

        foreach($options as $key => $value){
            if(null !== $value){
                $query .= "&" . $key . "=" . $value;
            }
        }

        return $query;
    }
}
