<?php

namespace Fungible\MoralisApi\EVM\V3;

trait Storage {
    /**
     * @param $data
     * 
     * @return array|null
     */
    public function uploadFolder($data = null)
    {
        try {
            $response = $this->post('/ipfs/uploadfolder', $data);
            $responseString = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseString;
    }
}