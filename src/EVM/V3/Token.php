<?php

namespace Fungible\MoralisApi\EVM\V3;

trait Token {
    /**
     * @param string $address
     * @param string $subDomain
     * @param string $providerUrl
     * 
     * @return array|null
     */
    public function getERC20Metadata(array $addresses, string $subDomain = null, string $providerUrl = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'providerUrl' => $providerUrl,
            'addresses' => count($addresses) ?  implode('&addresses=', $addresses) : null,
        ]);

        try {
            $response = $this->get('erc20/metadata'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param int $fromBlock
     * @param string $toBlock
     * @param string $fromDate
     * @param string $toDate
     * @param string $providerUrl
     * @param string $marketplace
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getContractTrades(string $address, int $fromBlock = null, string $toBlock = null, string $fromDate = null, string $toDate = null, string $providerUrl = null, string $marketplace = 'opensea', string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'from_block' => $fromBlock,
            'to_block' => $toBlock,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'providerUrl' => $providerUrl,
            'marketplace' => $marketplace,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('nft/'. $address. '/trades' . $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param int $days
     * @param string $providerUrl
     * @param string $marketplace
     * 
     * @return array|null
     */
    public function getLowestPrice(string $address, int $days = null, string $providerUrl = null, string $marketplace = 'opensea')
    {
        $query = $this->getQuery([
            'days' => $days,
            'providerUrl' => $providerUrl,
            'marketplace' => $marketplace,
        ]);

        try {
            $response = $this->get('nft/'. $address. '/lowestprice' . $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param array $symbols
     * @param string $subDomain
     * 
     * @return array|null
     */
    public function getTokenContractMetadata(array $symbols, string $subDomain = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'symbols' => count($symbols) ? implode('&symbols=', $symbols) : null,
        ]);

        try {
            $response = $this->get('erc20/metadata/symbols'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $providerUrl
     * @param string $exchange
     * @param int $toBlock
     * 
     * @return array|null
     */
    public function getTokenPrice(string $address, string $providerUrl = null, string $exchange = null, int $toBlock = null)
    {
        $query = $this->getQuery([
            'providerUrl' => $providerUrl,
            'exchange' => $exchange,
            'to_block' => $toBlock,
        ]);

        try {
            $response = $this->get('erc20/'. $address. '/price'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }
    
    /**
     * @param string $address
     * @param string $subDomain
     * @param int $fromBlock
     * @param int $toBlock
     * @param string $fromDate
     * @param string $toDate
     * @param int $offset
     * @param int $limit
     * 
     * @return array|null
     */
    public function getERC20TokenContractTransactions(string $address, string $subDomain = null, int $fromBlock = null, int $toBlock = null, string $fromDate = null, string $toDate = null, int $offset = null, int $limit = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'from_block' => $fromBlock,
            'to_block' => $toBlock,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'offset' => $offset,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('erc20/'. $address. '/transfers'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $ownerAddress
     * @param string $spenderAddress
     * @param string $address
     * @param string $providerUrl
     * 
     * @return array|null
     */
    public function getAllowance(string $ownerAddress, string $spenderAddress, string $address, string $providerUrl = null)
    {
        $query = $this->getQuery([
            'providerUrl' => $providerUrl,
            'owner_address' => $ownerAddress,
            'spender_address' => $spenderAddress,
        ]);

        try {
            $response = $this->get('erc20/'. $address. '/allowance'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;    
    }

    /**
     * @param string $searchQuery
     * @param string $format
     * @param string $filter
     * @param int $fromBlock
     * @param int $toBlock
     * @param int $fromDate
     * @param string $toDate
     * @param array $addresses
     * @param string $tokenAddress
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getNftData(string $searchQuery, string $format = 'decimal', string $filter = null, int $fromBlock = null, int $toBlock = null, string $fromDate = null, string $toDate = null, array $addresses = null, string $tokenAddress = null, string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'format' => $format,
            'q' => $searchQuery,
            'filter' => $filter,
            'from_block' => $fromBlock,
            'to_block' => $toBlock,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'addresses' => $addresses,
            'token_address' => $tokenAddress,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('/api/v2/nft/search'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $fromBlock
     * @param int $toBlock
     * @param int $fromDate
     * @param string $toDate
     * @param string $format
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getNftTransferData(int $fromBlock = null, int $toBlock = null, string $fromDate = null, string $toDate = null, string $format = 'decimal', string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'from_block' => $fromBlock,
            'to_block' => $toBlock,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'format' => $format,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('/api/v2/nft/transfers'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $format
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getContractNfts(string $address, string $format = 'decimal', string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'format' => $format,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('nft/'. $address. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $format
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getNftContractTransfers(string $address, string $format = 'decimal', string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'format' => $format,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('nft/'. $address. '/transfers'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $format
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getAllContractNftOwners(string $address, string $format = 'decimal', string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'format' => $format,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('nft/'. $address. '/owners'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;    
    }

    /**
     * @param string $address
     * 
     * @return array|null
     */
    public function getGlobalMetadata(string $address)
    {
        $query = $this->getQuery();

        try {
            $response = $this->get('nft/'. $address. '/metadata'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $tokenId
     * @param string $flag
     * @param string $mode
     * 
     * @return int|null
     */
    public function resyncNftMetadata(string $address, string $tokenId, string $flag = 'uri', string $mode ='async')
    {
        $query = $this->getQuery([
            'flag' => $flag,
            'mode' => $mode,
        ]);

        try {
            $response = $this->get('nft/' . $address . '/' . $tokenId . '/metadata/resync'. $query);
            $responseCode = $response->getStatusCode();
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseCode;
    }

    /**
     * @param string $contract
     * 
     * @return string|null
     */
    public function contractSync(string $contract)
    {
        $query = $this->getQuery();

        try {
            $response = $this->put('nft/' . $contract . '/sync'. $query);
            $response = $response->getReasonPhrase();
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $response;
    }

    /**
     * @param string $address
     * @param string $tokenId
     * @param string $format
     * 
     * @return array|null
     */
    public function getAllNftData(string $address, string $tokenId, string $format = 'decimal')
    {
        $query = $this->getQuery([
            'format' => $format,
        ]);

        try {
            $response = $this->get('nft/'. $address. '/'. $tokenId. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $tokenId
     * @param string $format
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getAllNftOwnersWithinContract(string $address, string $tokenId, string $format = 'decimal', string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'format' => $format,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('nft/'. $address. '/'. $tokenId. '/owners'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $tokenId
     * @param string $format
     * @param string $order
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getTokenTransfers(string $address, string $tokenId, string $format = 'decimal', string $order = null, string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'format' => $format,
            'order' => $order,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('nft/'. $address. '/'. $tokenId . '/transfers'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }
}