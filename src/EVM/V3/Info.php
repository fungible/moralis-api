<?php

namespace Fungible\MoralisApi\EVM\V3;

trait Info {
    /**
     * @return string|null
     */    
    public function getWeb3Version()
    {
        try {
            $response = $this->get('/web3/version');
            $responseString = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseString;
    }

    /**
     * @return array|null
     */
    public function getEndpointWeights()
    {
        try {
            $response = $this->get('info/endpointWeights');
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;  
    }
}