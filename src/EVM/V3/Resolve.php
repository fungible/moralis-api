<?php

namespace Fungible\MoralisApi\EVM\V3;

trait Resolve {
    /**
     * @param string $domain
     * @param string $currency
     * 
     * @return string|null
     */
    public function resolveDomain(string $domain, string $currency = null)
    {
        $query = $this->getQuery([
            'currency' => $currency,
        ]);

        try {
            $response = $this->get('resolve/'. $domain. $query);
            $responseString = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseString;
    }

    /**
     * @param string $address
     * 
     * @return string|null
     */    
    public function resolveEthAddress(string $address)
    {
        $query = $this->getQuery();

        try {
            $response = $this->get('resolve/'. $address. '/reverse'. $query);
            $responseString = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseString;
    }
}