<?php

namespace Fungible\MoralisApi\EVM\V3;

trait Defi {
    /**
     * @param string $pairAddress
     * @param string $toBlock
     * @param string $toDate
     * @param string $providerUrl
     * 
     * @return array|null
     */
    public function getPairAddressLiquidityReserves(string $pairAddress, string $toBlock = null, string $toDate = null, string $providerUrl = null)
    {
        $query = $this->getQuery([
            'to_block' => $toBlock,
            'to_date' => $toDate,
            'provider_url' => $providerUrl,
        ]);

        try {
            $response = $this->get($pairAddress. '/reserves'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $exchange
     * @param string $token0Addres
     * @param string $token1Addres
     * @param string $toBlock
     * @param string $toDate
     * 
     * @return array|null
     */    
    public function getPairAddress(string $exchange, string $token0Address, string $token1Address, string $toBlock = null, string $toDate = null)
    {
        $query = $this->getQuery([
            'exchange' => $exchange,
            'to_block' => $toBlock,
            'to_date' => $toDate
        ]);

        try {
            $response = $this->get($token0Address. '/'. $token1Address. '/pairAddress'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }
}