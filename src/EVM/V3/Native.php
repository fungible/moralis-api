<?php

namespace Fungible\MoralisApi\EVM\V3;

trait Native {
    /**
     * @param string $blockNumber
     * @param string $subDomain
     * 
     * @return array|null
     */
    public function getContentByBlockHash(string $blockNumber, string $subDomain = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
        ]);

        try {
            $response = $this->get('block/'. $blockNumber. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $date
     * @param string $providerUrl
     * 
     * @return array|null
     */
    public function getDateToBlock(string $date, string $providerUrl = null)
    {
        $query = $this->getQuery([
            'date' => $date,
            'proverUrl' => $providerUrl,
        ]);

        try {
            $response = $this->get('dateToBlock'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;  
    }

    /**
     * @param string $address
     * @param string $subDomain
     * @param string $blockNumber
     * @param string $fromBlock
     * @param string $toBlock
     * @param string $fromDate
     * @param string $toDate
     * @param string $topic0
     * @param string $topic1
     * @param string $topic2
     * @param string $topic3
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getAddressLogs(string $address, string $subDomain = null, string $blockNumber = null, string $fromBlock = null, string $toBlock = null, string $fromDate = null, string $toDate = null, string $topic0 = null, string $topic1 = null, string $topic2 = null, string $topic3 = null, string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'block_number' => $blockNumber,
            'from_block' => $fromBlock,
            'to_block' => $toBlock,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'topic0' => $topic0,
            'topic1' => $topic1,
            'topic2' => $topic2,
            'topic3' => $topic3,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get($address. '/logs'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $blockNumber
     * @param int $subDomain
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getBlockNftTransfers(string $blockNumber, int $subDomain = null, string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get('block/'. $blockNumber. '/nft/transfers'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $transactionHash
     * @param string $subDomain
     * 
     * @return array|null
     */
    public function getTransactionHash(string $transactionHash, string $subDomain = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
        ]);

        try {
            $response = $this->get('transaction/'. $transactionHash. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;  
    }

    /**
     * @param string $address
     * @param string $topic
     * @param array $abi
     * @param string $subDomain
     * @param string $providerUrl
     * @param int $fromBlock
     * @param int $toBlock
     * @param string $fromDate
     * @param string $toDate
     * @param int $offset
     * @param int $limit
     *
     * @return array|null
     */
    public function getAddressEvents(string $address, string $topic, array $abi, string $subDomain = null, string $providerUrl = null, int $fromBlock = null, int $toBlock = null, string $fromDate = null, string $toDate = null, int $offset = null, int $limit = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'providerUrl' => $providerUrl,
            'from_block' => $fromBlock,
            'to_block' => $toBlock,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'topic' => $topic,
            'offset' => $offset,
            'limit' => $limit,
        ]);

        try {
            $response = $this->post($address. '/events'. $query, $abi);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $functionName
     * @param array $abi
     * @param array $params
     * @param string $subDomain
     * @param string $providerUrl
     * 
     * @return array|null
     */
    public function runAbiFunction(string $address, string $functionName, array $abi, array $params = [], string $subDomain = null, string $providerUrl = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'providerUrl' => $providerUrl,
            'function_name' => $functionName,
        ]);

        $data = [
            'json' => [
                'abi' => $abi,
                'params' => $params,
            ]
        ];

        try {
            $response = $this->post($address. '/function'. $query, $data);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;  
    }
}