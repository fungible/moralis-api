<?php

namespace Fungible\MoralisApi\EVM\V3;

trait Account {
    /**
     * @param string $address
     * @param string $subDomain
     * @param int $fromBlock
     * @param int $toBlock
     * @param string $fromDate
     * @param string $toDate
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getNativeTransactions(string $address, string $subDomain = null, int $fromBlock = null, int $toBlock = null, string $fromDate = null, string $toDate = null, string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'from_block' => $fromBlock,
            'to_block' => $toBlock,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get($address. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $providerUrl
     * @param int $toBlock
     * 
     * @return array|null
     */
    public function getSpecificAddressBalance(string $address, string $providerUrl = null, int $toBlock = null)
    {
        $query = $this->getQuery([
            'providerUrl' => $providerUrl,
            'to_block' => $toBlock,
        ]);

        try {
            $response = $this->get($address. '/balance'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $subdomain
     * @param int $toBlock
     * @param array $tokenAddresses
     * 
     * @return array|null
     */
    public function getSpecificAddressTokenBalance(string $address, string $subDomain = null, int $toBlock = null, array $tokenAddresses = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'to_block' => $toBlock,
            'token_addresses' => count($tokenAddresses) ? implode('&token_addresses=', $tokenAddresses) : null,
        ]);

        try {
            $response = $this->get($address. '/erc20'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $subDomain
     * @param int $fromBlock
     * @param int $toBlock
     * @param string $fromDate
     * @param string $toDate
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getERC20TokenTransactions(string $address, string $subDomain = null, int $fromBlock = null, int $toBlock = null, string $fromDate = null, string $toDate = null, string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'subdomain' => $subDomain,
            'from_block' => $fromBlock,
            'to_block' => $toBlock,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get($address. '/erc20/transfers'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $format
     * @param array $tokenAddresses
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getUserAddressNfts(string $address, string $format = 'decimal', array $tokenAddresses = [], string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'format' => $format,
            'token_addresses' => count($tokenAddresses) ? implode('&token_addresses=', $tokenAddresses) : null,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get($address . '/nft' . $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $format
     * @param string $direction
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getNftTransfers(string $address, string $format = 'decimal', string $direction = null, string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'format' => $format,
            'direction' => $direction,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get($address . '/nft/transfers'. $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }

    /**
     * @param string $address
     * @param string $tokenAddress
     * @param string $format
     * @param string $cursor
     * @param int $limit
     * 
     * @return array|null
     */
    public function getNftsFromAddress(string $address, string $tokenAddress, string $format = 'decimal', string $cursor = null, int $limit = null)
    {
        $query = $this->getQuery([
            'format' => $format,
            'cursor' => $cursor,
            'limit' => $limit,
        ]);

        try {
            $response = $this->get($address . '/nft'. $tokenAddress . $query);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }
}