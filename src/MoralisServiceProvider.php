<?php

namespace Fungible\MoralisApi;

use Illuminate\Support\ServiceProvider;

class MoralisServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/moralis.php' => config_path('moralis.php')
        ]);
    }

    public function register()
    {
        $this->app->singleton(Moralis::class, function() {
            return new Moralis();
        });
    }
}
